package net.infobosccoma.movies.views.impl.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import net.infobosccoma.movies.R;
import net.infobosccoma.movies.helpers.YearsBuilder;

import net.infobosccoma.movies.models.business.entitties.Movie;
import net.infobosccoma.movies.models.business.entitties.Year;
import net.infobosccoma.movies.models.persistence.daos.impl.MovieSQLiteDAO;
import net.infobosccoma.movies.models.persistence.daos.impl.YearSQLiteDAO;
import net.infobosccoma.movies.views.impl.adapters.YearAdapter;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

public class MainActivityFirst extends AppCompatActivity implements AdapterView.OnItemClickListener{

    private ArrayList<Year> llistaYears;
    private YearAdapter adapterLlistaYear;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity_first);


        listView = (ListView) findViewById(R.id.listViewMoviesAcFirst);

        YearSQLiteDAO yearSQLite = new YearSQLiteDAO(this);
        llistaYears = (ArrayList<Year>) yearSQLite.getAllYears();

       // Funciona sense estirar-ho de la base de dades.
//        llistaYears = YearsBuilder.createYears();
//        YearsBuilder.addMoviesToYear(llistaYears);

        adapterLlistaYear = new YearAdapter(this, llistaYears);
        listView.setAdapter(adapterLlistaYear);
        registerForContextMenu(listView);
        listView.setOnItemClickListener(this);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent i = new Intent(this, ActivitySecond.class);
        Year year = (Year) listView.getItemAtPosition(position);
        List<Movie> movies = null;
        MovieSQLiteDAO movieDAO = new MovieSQLiteDAO(this);
        movies =  movieDAO.getAllMoviesForYear(year);
        year.setMovies(movies);
        i.putExtra("DADES_ANY", Parcels.wrap(year));
        startActivityForResult(i,1);
    }
}
