package net.infobosccoma.movies.views.impl.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import net.infobosccoma.movies.R;

import net.infobosccoma.movies.models.business.entitties.Movie;
import net.infobosccoma.movies.models.business.entitties.Year;
import net.infobosccoma.movies.views.impl.adapters.MovieAdapter;

import org.parceler.Parcels;

public class ActivitySecond extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private MovieAdapter adapterLlistaMovie;
    private ListView listView;
    private Year dades;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        //Agafar info de l'altre activity
        dades =  Parcels.unwrap(getIntent().getParcelableExtra("DADES_ANY"));


        listView = (ListView) findViewById(R.id.listView_SecondActivity);
        listView.setOnItemClickListener(this);


        adapterLlistaMovie = new MovieAdapter(this, dades.getMovies());
        listView.setAdapter(adapterLlistaMovie);
        registerForContextMenu(listView);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent i = new Intent(this, DetailActivity.class);
        Movie movie = (Movie) listView.getItemAtPosition(position);
        i.putExtra("DADES_PELI", Parcels.wrap(movie));
        startActivityForResult(i,1);
    }

}
