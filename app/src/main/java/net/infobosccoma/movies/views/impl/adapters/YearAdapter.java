package net.infobosccoma.movies.views.impl.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import net.infobosccoma.movies.R;
import net.infobosccoma.movies.models.business.entitties.Year;


import java.util.ArrayList;

/**
 * Created by Eduard on 16/02/2016.
 */



public class YearAdapter extends ArrayAdapter<Year> {

    private ArrayList<Year> dadesAnys;
    private Activity context;

    public YearAdapter(Activity context, ArrayList<Year> dades){
        super(context, R.layout.main_item_list_activity_first, dades);
        this.context = context;
        this.dadesAnys = dades;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View item = convertView;
        Vista vista;
        //no hi havia generada la vista amb anterioritat.
        if(item == null){
            LayoutInflater inflater = context.getLayoutInflater();
            item = inflater.inflate(R.layout.main_item_list_activity_first, null);

            vista = new Vista();
            vista.any = (TextView)item.findViewById(R.id.textView_item_activity_first);


            item.setTag(vista);

        }else{
            //la vista ja s'havia generat anteriorment.
            vista = (Vista)item.getTag();
        }


        vista.any.setText(dadesAnys.get(position).toString());


        return item;
    }
    class Vista{

        public TextView any;
    }
}
