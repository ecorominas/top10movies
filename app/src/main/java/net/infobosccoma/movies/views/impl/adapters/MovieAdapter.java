package net.infobosccoma.movies.views.impl.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import net.infobosccoma.movies.R;
import net.infobosccoma.movies.models.business.entitties.Movie;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eduard on 20/02/2016.
 */
public class MovieAdapter extends ArrayAdapter<Movie> {

    private List<Movie> dadesPelis;
    private Activity context;

    public MovieAdapter(Activity context, List<Movie> dades){

        super(context, R.layout.item_list_activity_second, dades);
        this.context = context;
        this.dadesPelis = dades;

    }



    public View getView(int position, View convertView, ViewGroup parent) {

        View item = convertView;
        Vista vista;
        //no hi havia generada la vista amb anterioritat.
        if(item == null){
            LayoutInflater inflater = context.getLayoutInflater();
            item = inflater.inflate(R.layout.item_list_activity_second, null);

            vista = new Vista();
            vista.any = (TextView)item.findViewById(R.id.textView_item_list__activity_second);


            item.setTag(vista);

        }else{
            //la vista ja s'havia generat anteriorment.
            vista = (Vista)item.getTag();
        }

        vista.any.setText(dadesPelis.get(position).getNameMovie());

        return item;
    }

    class Vista{

        public TextView any;
    }
}
