package net.infobosccoma.movies.views.impl.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import net.infobosccoma.movies.R;
import net.infobosccoma.movies.models.business.entitties.Descarregar;
import net.infobosccoma.movies.models.business.entitties.Movie;


import org.parceler.Parcels;

public class DetailActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    private ImageView image;
    private Movie peli;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        peli = Parcels.unwrap(getIntent().getParcelableExtra("DADES_PELI"));
        mostrarDades(peli);

    }

    private void mostrarDades(Movie peli) {

        TextView txtTitol = (TextView) findViewById(R.id.titolMovie_DetailActivity);
        txtTitol.setText(peli.getNameMovie());
        TextView txtDescripcio = (TextView) findViewById(R.id.descriptionMovie_DetailActivity);
        txtDescripcio.setText(peli.getDescriptionMovie());
        TextView txtValoracio = (TextView) findViewById(R.id.valorationMovie_DetailActivity);
        txtValoracio.setText(peli.getValorationMovie());
        TextView txtTitolValoracio = (TextView) findViewById(R.id.titolValoracioMovie_DetailActivity);
        txtTitolValoracio.setText("Valoració:");
        image = (ImageView) findViewById(R.id.imageView_DetailActivity);
        new Descarregar(image).execute(peli.getImage());

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

}
