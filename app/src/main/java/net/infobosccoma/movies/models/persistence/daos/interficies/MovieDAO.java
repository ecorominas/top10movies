package net.infobosccoma.movies.models.persistence.daos.interficies;

import net.infobosccoma.movies.models.business.entitties.Movie;
import net.infobosccoma.movies.models.business.entitties.Year;


import java.util.List;

/**
 * Created by Eduard on 01/03/2016.
 */
public interface MovieDAO {

    List<Movie> getAllMovies();
    List<Movie> getAllMoviesForYear(Year year);
    boolean save(Movie movie);
    boolean update(Movie movie);
    boolean delete(Movie movie);
}
