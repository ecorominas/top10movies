package net.infobosccoma.movies.models.persistence.daos.impl;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import net.infobosccoma.movies.models.business.entitties.Movie;
import net.infobosccoma.movies.models.business.entitties.Year;
import net.infobosccoma.movies.models.persistence.daos.interficies.YearDAO;
import net.infobosccoma.movies.models.persistence.utilities.SQLiteUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eduard on 01/03/2016.
 */
public class YearSQLiteDAO implements YearDAO {

    private Context context;

    public YearSQLiteDAO(Context context) {

        this.context = context;
    }


    @Override
    public List<Year> getAllYears() {
        List<Year> years = new ArrayList<>();

        SQLiteDatabase conn = SQLiteUtils.getConnection(context);

        Cursor result = conn.query(true, "Movies", new String[]{"year"}, null, null, null, null, null, null);

        while (result.moveToNext()) {

            years.add(SQLiteUtils.getYear(result));
        }

        conn.close();
        return years;
    }


}
