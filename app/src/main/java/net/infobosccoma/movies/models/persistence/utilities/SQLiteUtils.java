package net.infobosccoma.movies.models.persistence.utilities;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import net.infobosccoma.movies.models.business.entitties.Movie;
import net.infobosccoma.movies.models.business.entitties.Year;

/**
 * Created by Eduard on 01/03/2016.
 */
public class SQLiteUtils {

    static final String NOM_BD = "top10movies.db";
    static final int VERSIO_BD = 2;


    public static SQLiteDatabase getConnection(Context context) {

        return new YearMovieSQLiteHelper(context, NOM_BD, null, VERSIO_BD).getWritableDatabase();
    }

    public static Year getYear(Cursor reader) {

        Year year = new Year();

        year.setAny(reader.getInt(reader.getColumnIndex("year")));

        return year;
    }

    public static Movie getMovie(Cursor reader) {

       Movie movie = new Movie();

        movie.setNameMovie(reader.getString(reader.getColumnIndex("name")));
        movie.setDescriptionMovie(reader.getString(reader.getColumnIndex("description")));
        movie.setValorMovie(reader.getString(reader.getColumnIndex("valor")));
        movie.setImage(reader.getString(reader.getColumnIndex("image")));

        return movie;
    }
}