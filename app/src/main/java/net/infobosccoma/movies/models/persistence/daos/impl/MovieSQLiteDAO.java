package net.infobosccoma.movies.models.persistence.daos.impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import net.infobosccoma.movies.models.business.entitties.Movie;
import net.infobosccoma.movies.models.business.entitties.Year;
import net.infobosccoma.movies.models.persistence.daos.interficies.MovieDAO;
import net.infobosccoma.movies.models.persistence.utilities.SQLiteUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eduard on 01/03/2016.
 */
public class MovieSQLiteDAO implements MovieDAO {

    private Context context;

    public MovieSQLiteDAO(Context context) {

        this.context = context;
    }


    @Override
    public List<Movie> getAllMovies() {
        List<Movie> movies = new ArrayList<>();

        SQLiteDatabase conn = SQLiteUtils.getConnection(context);

        Cursor result = conn.query(true, "Movies", new String[]{"name", "description", "valor","image"}, null, null, null, null, null, null);

        while (result.moveToNext()) {
            movies.add(SQLiteUtils.getMovie(result));
        }

        conn.close();
        return movies;
    }

    @Override
    public List<Movie> getAllMoviesForYear(Year year) {
        List<Movie> movies = new ArrayList<>();

        SQLiteDatabase conn = SQLiteUtils.getConnection(context);

        Cursor result = conn.query(true, "Movies", new String[]{"name", "description", "valor","image"}, "year = ?", new String[]{year.toString()}, null, null, null, null);

        while (result.moveToNext()) {
            movies.add(SQLiteUtils.getMovie(result));
        }

        conn.close();
        return movies;
    }

    @Override
    public boolean save(Movie movie) {
        ContentValues dades = new ContentValues();
        SQLiteDatabase conn = SQLiteUtils.getConnection(context);
        dades.put("name", movie.getNameMovie());
        dades.put("description", movie.getDescriptionMovie());
        dades.put("valor", movie.getValorMovie());
        dades.put("image", movie.getImage());


        try {
            long codi = conn.insertOrThrow("Movies", null, dades);
            movie.setCodi(codi);
            return true;

        } catch (Exception ex) {
            Log.e("Movies", ex.getMessage());
            return false;
        }
    }

    @Override
    public boolean update(Movie movie) {
        ContentValues dades = new ContentValues();
        SQLiteDatabase conn = SQLiteUtils.getConnection(context);
        dades.put("name", movie.getNameMovie());
        dades.put("description", movie.getDescriptionMovie());
        dades.put("valor", movie.getValorMovie());
        dades.put("image", movie.getImage());

        return conn.update("Movies", dades, "codi = ?", new String[]{String.valueOf(movie.getCodi())}) > 0;
    }

    @Override
    public boolean delete(Movie movie) {
        SQLiteDatabase conn = SQLiteUtils.getConnection(context);

        return conn.delete("Movies", "codi = ?", new String[]{String.valueOf(movie.getCodi())}) > 0;
    }
}
