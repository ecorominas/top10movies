package net.infobosccoma.movies.models.business.entitties;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eduard on 16/02/2016.
 */
@Parcel
public class Year {


    private int any;
    private List<Movie> movies;

    public Year(){

    }

    public Year(int any){

        this.any = any;
        this.movies = new ArrayList<Movie>();
    }

    public int getAny() {
        return any;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public void setAny(int any) {
        this.any = any;
    }

    public void addMovie(Movie movie) {
        movies.add(movie);
    }

    public void addMovieList(ArrayList<Movie> list) {
        movies.addAll(list);
    }


    @Override
    public String toString() {

        return String.format("%d", any);
    }
}
