package net.infobosccoma.movies.models.persistence.daos.interficies;


import net.infobosccoma.movies.models.business.entitties.Year;

import java.util.List;

/**
 * Created by Eduard on 29/02/2016.
 */
public interface YearDAO {

    List<Year> getAllYears();

}
