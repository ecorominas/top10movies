package net.infobosccoma.movies.models.business.entitties;

import org.parceler.Parcel;


/**
 * Created by Eduard on 15/02/2016.
 */

@Parcel
public class Movie{

    private long codi;
    private String nameMovie;
    private String descriptionMovie;
    private String valorMovie;
    private String image;

    public Movie() {

    }


    public Movie(String nameMovie, String descriptionMovie, String valorMovie, String image){


        this.nameMovie = nameMovie;
        this.descriptionMovie = descriptionMovie;
        this.valorMovie = valorMovie;
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getValorMovie() {
        return valorMovie;
    }

    public void setValorMovie(String valorMovie) {
        this.valorMovie = valorMovie;
    }

    public String getNameMovie() {
        return nameMovie;
    }

    public void setNameMovie(String nameMovie) {
        this.nameMovie = nameMovie;
    }

    public String getValorationMovie() {
        return valorMovie;
    }

    public void setValorationMovie(String valorationMovie) {
        this.valorMovie = valorationMovie;
    }

    public String getDescriptionMovie() {
        return descriptionMovie;
    }

    public void setDescriptionMovie(String descriptionMovie) {
        this.descriptionMovie = descriptionMovie;
    }
    public long getCodi() {
        return codi;
    }

    public void setCodi(long codi) {
        this.codi = codi;
    }
}
