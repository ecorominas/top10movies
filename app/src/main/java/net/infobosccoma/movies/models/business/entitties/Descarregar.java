package net.infobosccoma.movies.models.business.entitties;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Eduard on 22/02/2016.
 */
public class Descarregar extends AsyncTask<String, Void, Bitmap> {

    private ImageView image;

    public Descarregar(ImageView url){

        this.image = url;
    }


    private Bitmap loadImageFromNetwork(String imageURL) {

        try {

            Bitmap bitmap = BitmapFactory.decodeStream((InputStream) new URL(imageURL).getContent());
            return bitmap;

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected Bitmap doInBackground(String... urls) {
        return loadImageFromNetwork(urls[0]);
    }
    @Override
    protected void onPostExecute(Bitmap bitmap) {

        image.setImageBitmap(bitmap);
    }
}

